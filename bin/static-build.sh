#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -x
# project root
cd "$(dirname "$DIR")"

export CC_x86_64_unknown_linux_musl="x86_64-linux-musl-gcc"
cargo build --release --target=x86_64-unknown-linux-musl
