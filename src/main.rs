use std::{io, net::TcpListener};

use actix_web::{Error as ActixErr, HttpResponse, HttpServer};
use anyhow::Result;

mod config;

#[actix_web::get("/")]
async fn index() -> Result<HttpResponse, ActixErr> {
    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .body(&include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/index.html"))[..]))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let app_config =
        config::cli_args().map_err(|e| io::Error::new(io::ErrorKind::InvalidInput, e))?;

    let listener = TcpListener::bind(&app_config.bind_addr)?;
    println!("Starting server at {}", listener.local_addr()?);

    HttpServer::new(|| actix_web::App::new().service(index))
        .listen(listener)?
        .run()
        .await
}
