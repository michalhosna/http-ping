use std::net::{SocketAddr, ToSocketAddrs};

use anyhow::Context;

#[derive(Clone, Debug)]
pub struct AppConfig {
    pub bind_addr: SocketAddr,
}

pub fn cli_args() -> anyhow::Result<AppConfig> {
    use clap::{App, Arg};

    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(concat!(
            env!("CARGO_PKG_VERSION"),
            " git:",
            env!("VERGEN_GIT_SHA")
        ))
        .long_version(concat!(
            env!("CARGO_PKG_VERSION"),
            "\n",
            "\nCommit SHA:\t",
            env!("VERGEN_GIT_SHA"),
            "\nCommit Date:\t",
            env!("VERGEN_GIT_COMMIT_TIMESTAMP"),
            "\nrustc Version:\t",
            env!("VERGEN_RUSTC_SEMVER"),
            "\nrustc SHA:\t",
            env!("VERGEN_RUSTC_COMMIT_HASH"),
            "\ncargo Target:\t",
            env!("VERGEN_CARGO_TARGET_TRIPLE"),
            "\ncargo Profile:\t",
            env!("VERGEN_CARGO_PROFILE"),
        ))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(concat!(
            env!("CARGO_PKG_DESCRIPTION"),
            "\n\n",
            env!("CARGO_PKG_REPOSITORY")
        ))
        .arg(
            Arg::with_name("bind-addr")
                .long("bind-addr")
                .takes_value(true)
                .help("IP Address to bind to, e.g. 0.0.0.0:8080")
                .required(true),
        )
        .get_matches();

    let bind_addr = matches
        .value_of("bind-addr")
        .expect("Could not get value of bind-addr")
        .to_socket_addrs()
        .with_context(|| "Failed to parse bind-addr")?
        .next()
        .with_context(|| "Failed to obtain bind-addr")?;

    Ok(AppConfig { bind_addr })
}
